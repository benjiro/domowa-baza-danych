package com.liguz.database.service;

import com.liguz.database.domain.User;

public interface UserService {
	public void createUser(User user);
	public User getUserByUsername(String username);

}
