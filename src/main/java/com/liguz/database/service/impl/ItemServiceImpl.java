package com.liguz.database.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liguz.database.domain.Item;
import com.liguz.database.domain.repository.ItemRepository;
import com.liguz.database.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository itemRepository;

	public List<Item> getAllProducts() {
		return itemRepository.getAllProducts();
	}

	public void insertItemToDatabase(Item itemToBeAdded) {
		itemRepository.insertItemToDatabase(itemToBeAdded);
	}

	public void deleteItemFromDatabase(long id) {
		itemRepository.deleteItemFromDatabase(id);

	}

	public void updateItemInDatabase(Item itemToBeUpdated) {
		itemRepository.updateItemInDatabase(itemToBeUpdated);
	}

	@Override
	public void addToCart(long id, String username) {
		itemRepository.addToCart(id, username);
	}

	@Override
	public List<Item> getItemsByUser(String username) {
		return itemRepository.getItemsByUser(username);
	}

	@Override
	public void giveItemBack(long id, String username) {
		itemRepository.giveItemBack(id, username);
	}

	@Override
	public boolean checkingIfItemAlreadyInCart(long id, String username) {
		return itemRepository.checkingIfItemAlreadyInCart(id, username);
	}

	@Override
	public boolean noItemsInRepository(long id) {
		return itemRepository.noItemsInRepository(id);
	}


}
