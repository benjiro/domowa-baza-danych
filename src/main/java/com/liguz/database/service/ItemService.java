package com.liguz.database.service;

import java.util.List;

import com.liguz.database.domain.Item;

public interface ItemService {
	List<Item> getAllProducts();
	void insertItemToDatabase(Item itemToBeAdded);
	void deleteItemFromDatabase(long id);
	void updateItemInDatabase(Item itemToBeUpdated);
	void addToCart(long id, String username);
	List<Item> getItemsByUser(String username);
	void giveItemBack(long id, String username);
	boolean checkingIfItemAlreadyInCart(long id, String username);
	boolean noItemsInRepository(long id);

}
