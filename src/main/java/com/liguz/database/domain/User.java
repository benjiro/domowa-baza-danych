package com.liguz.database.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.liguz.database.validator.Username;

@Entity
@Table(name = "użytkownicy")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToMany
	@JoinTable(name = "przedmioty_użytkowników", joinColumns = {
			@JoinColumn(name = "id_użytkownika") }, inverseJoinColumns = { @JoinColumn(name = "id_przedmiotu") })
	private List<Item> items;

	@Username
	@Size(min = 3, max = 20, message = "{Size.User.username.Validation}")
	@Column(name = "nazwa")
	private String username;

	@Size(min = 3, max = 20, message = "{Size.User.password.Validation}")
	@Column(name = "hasło")
	private String password;

	@Column(name = "uprawnienia")
	private String authority;

	public User() {
		super();
	}

	public User(String username, String password, String authority) {
		this.authority = authority;
		this.password = password;
		this.username = username;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
