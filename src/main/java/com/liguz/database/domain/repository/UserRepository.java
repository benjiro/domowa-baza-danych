package com.liguz.database.domain.repository;

import com.liguz.database.domain.User;

public interface UserRepository {
	public void createUser(User user);
	public User getUserByUsername(String username);
}
