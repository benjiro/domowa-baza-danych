package com.liguz.database.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.spi.PersistenceProvider;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.stereotype.Repository;

import com.liguz.database.domain.Item;
import com.liguz.database.domain.User;
import com.liguz.database.domain.repository.ItemRepository;

@Repository
public class ItemRepositoryImpl implements ItemRepository {

	public List<Item> getAllProducts() {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TypedQuery<Item> query = entityManager.createQuery("SELECT i FROM Item i", Item.class);
		return query.getResultList();
	}

	public void insertItemToDatabase(Item itemToBeAdded) {
		EntityManagerFactory entityManagerFactory;

		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		itemToBeAdded.setAvailableItems(itemToBeAdded.getAmount());
		entityManager.getTransaction().begin();
		entityManager.persist(itemToBeAdded);
		entityManager.flush();
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public void deleteItemFromDatabase(long id) {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TypedQuery<Item> query = entityManager.createQuery("SELECT i FROM Item i WHERE i.id =:idItem", Item.class);
		query.setParameter("idItem", id);

		Item it = query.getSingleResult();

		entityManager.getTransaction().begin();
		entityManager.remove(it);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public void updateItemInDatabase(Item itemToBeUpdated) {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		entityManager.getTransaction().begin();
		entityManager.merge(itemToBeUpdated);
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Override
	public void addToCart(long id, String username) {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TypedQuery<Item> queryItem = entityManager.createQuery("SELECT i FROM Item i WHERE i.id =:idItem", Item.class);
		queryItem.setParameter("idItem", id);
		TypedQuery<User> queryUser = entityManager.createQuery("SELECT u FROM User u WHERE u.username =:usernameUser",
				User.class);
		queryUser.setParameter("usernameUser", username);
		User user = queryUser.getSingleResult();
		Item item = queryItem.getSingleResult();
		List<User> users = new ArrayList<User>();
		List<Item> items = new ArrayList<Item>();

		if (item.getAvailableItems() > 0) {
			item.setAvailableItems(item.getAvailableItems() - 1);
			users.add(user);
			item.setUsers(users);
			items.add(item);
			user.setItems(items);
			entityManager.getTransaction().begin();
			entityManager.merge(item);
			entityManager.merge(user);
			entityManager.getTransaction().commit();
			// entityManager.refresh(queryUser.getSingleResult());
			entityManager.close();
		}
	}

	@Override
	public List<Item> getItemsByUser(String username) {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TypedQuery<User> queryUser = entityManager.createQuery("SELECT u FROM User u WHERE u.username =:usernameUser",
				User.class);
		queryUser.setParameter("usernameUser", username);

		User user = queryUser.getSingleResult();

		return user.getItems();
	}

	@Override
	public void giveItemBack(long id, String username) {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TypedQuery<Item> queryItem = entityManager.createQuery("SELECT i FROM Item i WHERE i.id =:idItem", Item.class);
		queryItem.setParameter("idItem", id);
		TypedQuery<User> queryUser = entityManager.createQuery("SELECT u FROM User u WHERE u.username =:usernameUser",
				User.class);
		queryUser.setParameter("usernameUser", username);
		User user = queryUser.getSingleResult();
		Item item = queryItem.getSingleResult();
		List<User> users = item.getUsers();
		List<Item> items = user.getItems();
		items.remove(item);
		users.remove(user);

		item.setAvailableItems(item.getAvailableItems() + 1);

		entityManager.getTransaction().begin();
		entityManager.merge(item);
		entityManager.merge(user);
		entityManager.getTransaction().commit();
		entityManager.close();

	}

	@Override
	public boolean checkingIfItemAlreadyInCart(long id, String username) {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TypedQuery<Item> queryItem = entityManager.createQuery("SELECT i FROM Item i WHERE i.id =:idItem", Item.class);
		queryItem.setParameter("idItem", id);
		TypedQuery<User> queryUser = entityManager.createQuery("SELECT u FROM User u WHERE u.username =:usernameUser",
				User.class);
		queryUser.setParameter("usernameUser", username);
		User user = queryUser.getSingleResult();
		Item item = queryItem.getSingleResult();
		List<Item> listOfUsersItems = user.getItems();
		if (listOfUsersItems.contains(item))
			return true;
		else
			return false;
	}

	@Override
	public boolean noItemsInRepository(long id) {
		EntityManagerFactory entityManagerFactory;
		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		TypedQuery<Item> queryItem = entityManager.createQuery("SELECT i FROM Item i WHERE i.id =:idItem", Item.class);
		queryItem.setParameter("idItem", id);
		Item item = queryItem.getSingleResult();
		if (item.getAvailableItems() == 0)
			return true;
		else
			return false;
	}

}
