package com.liguz.database.domain.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.spi.PersistenceProvider;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.stereotype.Repository;

import com.liguz.database.domain.Item;
import com.liguz.database.domain.User;
import com.liguz.database.domain.repository.UserRepository;

@Repository
public class UserRepositoryImpl implements UserRepository{

	@Override
	public void createUser(User user) {
		EntityManagerFactory entityManagerFactory;

		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();

		entityManager.getTransaction().begin();
		entityManager.persist(user);
		entityManager.flush();
		entityManager.getTransaction().commit();
		entityManager.close();
		
	}

	@Override
	public User getUserByUsername(String username) {
		User userByUsername = null;
		EntityManagerFactory entityManagerFactory;

		PersistenceProvider provider = new HibernatePersistenceProvider();
		entityManagerFactory = provider.createEntityManagerFactory("mojabaza", null);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.username =:usernameUser", User.class);
		query.setParameter("usernameUser", username);
		
		if(query.getSingleResult() != null && query.getSingleResult().getUsername().equals(username)){
			userByUsername = query.getSingleResult();
		}
		
		
		return userByUsername;
	}
	
}
