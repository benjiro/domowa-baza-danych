package com.liguz.database.domain.repository;

import java.util.List;

import com.liguz.database.domain.Item;

public interface ItemRepository {
	List<Item> getAllProducts();
	void insertItemToDatabase(Item itemToBeAdded);
	void deleteItemFromDatabase(long id);
	void updateItemInDatabase(Item itemToBeUpdated);
	void addToCart(long id, String username);
	List<Item> getItemsByUser(String username);
	void giveItemBack(long id, String username);
	boolean checkingIfItemAlreadyInCart(long id, String username);
	boolean noItemsInRepository(long id);
}
