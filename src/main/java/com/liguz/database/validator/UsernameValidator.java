package com.liguz.database.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.liguz.database.domain.User;
import com.liguz.database.service.UserService;

public class UsernameValidator implements ConstraintValidator<Username, String>{

	@Autowired
	private UserService userService;
	public void initialize(Username constraintAnnotation){
		
	}
	public boolean isValid(String value, ConstraintValidatorContext context){
		User user;
		try{
			user = userService.getUserByUsername(value);
		}
		catch(Exception e){
			return true;
		}
		if(user!=null){
			return false;
		}
		return true;
	}
}
