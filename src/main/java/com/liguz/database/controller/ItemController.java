package com.liguz.database.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.liguz.database.domain.Item;
import com.liguz.database.service.ItemService;

@Controller
@RequestMapping("/")
public class ItemController {

	@Autowired
	ItemService itemService;

	@RequestMapping("/crud")
	public String welcome(Model model) {
		model.addAttribute("items", itemService.getAllProducts());
		return "crud";
	}

	@RequestMapping(value = "/crud/update", method = RequestMethod.GET)
	public String updateItem(@RequestParam("id") long id, Model model) {
		Item updateItem = new Item();
		model.addAttribute("newItem", updateItem);
		return "create";
	}

	@RequestMapping(value = "/crud/update", method = RequestMethod.POST)
	public String processUpdateItemForm(@ModelAttribute("newItem") Item itemToBeUpdated) {
		itemService.updateItemInDatabase(itemToBeUpdated);
		return "redirect:/crud";
	}

	@RequestMapping(value = "/crud/delete")
	public String deleteItem(@RequestParam("id") long id) {
		itemService.deleteItemFromDatabase(id);
		return "redirect:/crud";
	}

	@RequestMapping(value = "/crud/add", method = RequestMethod.GET)
	public String getAddNewItemForm(Model model) {
		Item newItem = new Item();
		model.addAttribute("newItem", newItem);
		return "create";
	}

	@RequestMapping(value = "/crud/add", method = RequestMethod.POST)
	public String processAddNewItemForm(@ModelAttribute("newItem") Item itemToBeAdded) {
		itemService.insertItemToDatabase(itemToBeAdded);
		return "redirect:/crud";
	}

	@RequestMapping(value = "/crud/itemtouser")
	public String addItemToUser(@RequestParam("id") long id, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		if (itemService.checkingIfItemAlreadyInCart(id, name))
			return "redirect:/crud/the-same-item";
		if (itemService.noItemsInRepository(id))
			return "redirect:/crud/no-more-items";
		else {
			itemService.addToCart(id, name);
			return "redirect:/crud";
		}
	}

	@RequestMapping(value = "/crud/the-same-item")
	public String informUserTakeTheSameItem(Model model) {
		model.addAttribute("error", "true");
		model.addAttribute("items", itemService.getAllProducts());
		return "crud";
	}
	
	@RequestMapping(value = "/crud/no-more-items")
	public String informUserNoMoreItems(Model model) {
		model.addAttribute("zero", "true");
		model.addAttribute("items", itemService.getAllProducts());
		return "crud";
	}
}
