package com.liguz.database.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.liguz.database.service.ItemService;

@Controller
public class UserController {
	@Autowired
	ItemService itemService;
	
	@RequestMapping("/cart")
	public String welcome(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String name = auth.getName();
		model.addAttribute("items", itemService.getItemsByUser(name));
		return "cart";
	}
	
	@RequestMapping(value = "/cart/itemtolibrary")
	public String addItemToUser(@RequestParam("id") long id, Model model){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String name = auth.getName();
	    itemService.giveItemBack(id, name);
		return "redirect:/cart/give-back";
	}
	
	@RequestMapping("/cart/give-back")
	public String itemReturned(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String name = auth.getName();
		model.addAttribute("items", itemService.getItemsByUser(name));
		model.addAttribute("back", "true");
		return "cart";
	}
}
