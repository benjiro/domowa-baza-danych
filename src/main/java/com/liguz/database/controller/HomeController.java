package com.liguz.database.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.liguz.database.domain.User;
import com.liguz.database.service.UserService;

@Controller
public class HomeController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Model model) {
		// model.addAttribute("notlogged", "true");
		return "welcome";
	}

	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String loginerror(Model model) {
		model.addAttribute("error", "true");
		return "welcome";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model) {
		model.addAttribute("logout", "true");
		return "welcome";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String getAddNewUserForm(Model model) {
		User newUser = new User();
		model.addAttribute("newUser", newUser);
		return "registration";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String processAddNewUserForm(@Valid @ModelAttribute("newUser") User userToBeAdded, BindingResult result,
			HttpServletRequest request) {
		if(result.hasErrors()){
			return "registration";
		}
		userToBeAdded.setAuthority("ROLE_USER");
		userService.createUser(userToBeAdded);
		return "redirect:/";
	}

}
