<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<head>
<title>Domowa baza danych</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"
	rel='stylesheet' type='text/css'>


<link href="<c:url value="/resource/css/table.css" />" rel="stylesheet">
<link
	href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css"
	rel="stylesheet" media="screen">
</head>
<body>
	<div class="container">
		<header class="row">
			<div class="span12">
				<nav class="navbar">
					<div class="navbar-inner">
						<a href="<spring:url value="/crud"/>" class="brand">Baza
							danych</a> <a class="btn btn-navbar" data-toggle="collapse"
							data-target=".nav-collapse"> <span class="icon-bar"></span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
						</a>
						<div class="nav-collapse collapse">
							<ul class="nav">
								<li class="divider-vertical"></li>
								<li><a href="<spring:url value="/"/>"><i
										class="icon-home"></i> Strona startowa</a></li>
								<li class="divider-vertical"></li>
								<li><a href="<spring:url value="/cart"/>">Wypożyczone
										przedmioty</a></li>
								<li class="divider-vertical"></li>
								<li><a href="#"><i class="icon-info-sign"></i> About</a></li>
								<li class="divider-vertical"></li>
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown"><i class="icon-user"></i> Connect <b
										class="caret"></b> </a>
									<ul class="dropdown-menu">
										<li><a href="#">Twitter</a></li>
										<li><a href="#">Facebook</a></li>
										<li><a href="#">Google+</a></li>
										<li class="divider"></li>
										<li><a href="#">Contact</a></li>
									</ul></li>
								<li class="divider-vertical"></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<!--END OF HEADER SECTION-->
		<div class="row">
			<a href="<c:url value="/j_spring_security_logout"/>"
				class="btn btn-danger pull-right">Wyloguj się</a>
			<p></p>
			<h1>Domowa baza danych</h1>
			<p>Zawiera przedmioty znajdujące się w moim wyposażeniu.</p>
			<p> </p>
			<p> </p>

			<div class="col-md-10 col-md-offset-1">

				<div class="panel panel-default panel-table">
					<div class="panel-heading">
						<div class="row">
							<div class="col col-xs-6">
								<h3 class="panel-title">Spis przedmiotów</h3>
							</div>
							<div class="caption">
								<sec:authorize access="hasRole('ROLE_ADMIN')">
									<p>
										<a href="<spring:url value="/crud/add" />"
											class="btn btn-primary"> <span
											class="clyphigon-info-sign glyphicon"></span>Dodaj przedmiot
										</a>
									</p>
								</sec:authorize>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<c:if test="${not empty error }">
							<div class="alert alert-danger">
								Już masz dany przedmiot, wybierz inny <br />
							</div>
						</c:if>
						<c:if test="${not empty zero }">
							<div class="alert alert-danger">
								Nie ma dostępnego danego produktu <br />
							</div>
						</c:if>
						<table class="table table-striped table-bordered table-list">
							<thead>
								<tr>
									<th><em class="fa fa-cog"></em></th>
									<th class="hidden-xs">ID</th>
									<th>Nazwa</th>
									<th>Kategoria</th>
									<th>Liczba</th>
									<th>Opis</th>
									<th>Liczba dostepnych produktów</th>
								</tr>
							</thead>
							<c:forEach items="${items}" var="item">
								<tbody>
									<tr>
										<td align="center"><sec:authorize
												access="hasRole('ROLE_ADMIN')">
												<a href="<spring:url value="/crud/update?id=${item.id}" />"
													class="btn btn-default"><em class="fa fa-pencil"></em></a>
												<a href="<spring:url value="/crud/delete?id=${item.id}" />"
													class="btn btn-danger"><em class="fa fa-trash"></em></a>
											</sec:authorize> <a
											href="<spring:url value="/crud/itemtouser?id=${item.id}" />"
											class="btn btn-success">+</a></td>
										<td class="hidden-xs">${item.id }</td>
										<td>${item.name }</td>
										<td>${item.category }</td>
										<td>${item.amount }</td>
										<td>${item.description}</td>
										<td>${item.availableItems}</td>
									</tr>
								</tbody>
							</c:forEach>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>