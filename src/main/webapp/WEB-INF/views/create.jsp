<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Domowa baza danych</title>
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link
	href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css"
	rel="stylesheet" media="screen">
<style>
body {
	background-image: url(resource/images/back-blue.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}
</style>
</head>
<body>

	<div class="container">

		<header class="row">
			<div class="span12">
				<nav class="navbar">
					<div class="navbar-inner">
						<a href="<spring:url value="/crud"/>" class="brand">Baza
							danych</a> <a class="btn btn-navbar" data-toggle="collapse"
							data-target=".nav-collapse"> <span class="icon-bar"></span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
						</a>
						<div class="nav-collapse collapse">
							<ul class="nav">
								<li class="divider-vertical"></li>
								<li><a href="<spring:url value="/"/>"><i
										class="icon-home"></i> Strona startowa</a></li>
								<li class="divider-vertical"></li>
								<li><a href="<spring:url value="/cart"/>">Wypożyczone
										przedmioty</a></li>
								<li class="divider-vertical"></li>
								<li><a href="#"><i class="icon-info-sign"></i> About</a></li>
								<li class="divider-vertical"></li>
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown"><i class="icon-user"></i> Connect <b
										class="caret"></b> </a>
									<ul class="dropdown-menu">
										<li><a href="#">Twitter</a></li>
										<li><a href="#">Facebook</a></li>
										<li><a href="#">Google+</a></li>
										<li class="divider"></li>
										<li><a href="#">Contact</a></li>
									</ul></li>
								<li class="divider-vertical"></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<!--END OF HEADER SECTION-->
		<section class="container">
			<form:form modelAttribute="newItem" class="form-horizontal">
				<fieldset>
					<legend>Dodaj nowy przedmiot</legend>
					<div class="control-group">
						<label class="control-label col-lg-2 col-lg-2" for="name">Nazwa</label>
						<div class="col-lg-10">
							<form:input id="name" path="name" type="text"
								class="form:input-large" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-lg-2 col-lg-2" for="category">Kategoria</label>
						<div class="col-lg-10">
							<form:select path="category" multiple="false">
								<option>Książki</option>
								<option>Elektronika</option>
								<option>Inne</option>
							</form:select>
							<%-- <form:input id="category" path="category" type="text"
								class="form:input-large" /> --%>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-lg-2 col-lg-2" for="amount">Liczba</label>
						<div class="col-lg-10">
							<form:input id="amount" path="amount" type="text"
								class="form:input-large" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label col-lg-2" for="description">Opis</label>
						<div class="col-lg-10">
							<form:textarea id="description" path="description" type="text"
								rows="2" />
						</div>
					</div>
					<div class="control-group">
						<div class="col-lg-offset-2 col-lg-10">
							<input type="submit" id="btnAdd" class="btn btn-primary"
								value="Dodaj" />
						</div>
					</div>
				</fieldset>
			</form:form>
		</section>
	</div>
</body>
</html>