<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Domowa baza danych</title>
<!-- Bootstrap -->
<!--<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">-->
<link
	href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css"
	rel="stylesheet" media="screen">
<style>
body {
	background-image: url(resource/images/back-blue.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
}
</style>

</head>
<body>

	<div class="container">

		<header class="row">
			<div class="span12">
				<nav class="navbar">
					<div class="navbar-inner">
						<a href="<spring:url value="/crud"/>" class="brand">Baza
							danych</a> <a class="btn btn-navbar" data-toggle="collapse"
							data-target=".nav-collapse"> <span class="icon-bar"></span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
						</a>
						<div class="nav-collapse collapse">
							<ul class="nav">
								<li class="divider-vertical"></li>
								<li><a href="<spring:url value="/"/>"><i
										class="icon-home"></i> Strona startowa</a></li>
								<li class="divider-vertical"></li>
								<li><a href="<spring:url value="/cart"/>">Wypożyczone
										przedmioty</a></li>
								<li class="divider-vertical"></li>
								<li><a href="#"><i class="icon-info-sign"></i> About</a></li>
								<li class="divider-vertical"></li>
								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown"><i class="icon-user"></i> Connect <b
										class="caret"></b> </a>
									<ul class="dropdown-menu">
										<li><a href="#">Twitter</a></li>
										<li><a href="#">Facebook</a></li>
										<li><a href="#">Google+</a></li>
										<li class="divider"></li>
										<li><a href="#">Contact</a></li>
									</ul></li>
								<li class="divider-vertical"></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<!--END OF HEADER SECTION-->

		<div class="row" id="main-content">
			<div class="span4" id="sidebar">
				<div class="panel-body">
					<c:if test="${not empty error }">
						<div class="alert alert-danger">
							Niepoprawna nazwa uzytkownika lub haslo <br />
						</div>
					</c:if>
					<c:if test="${not empty logout }">
						<div class="alert alert-success">
							Wylogowałeś się poprawnie<br />
						</div>
					</c:if>
					<%-- <c:if test="${not empty notlogged }">
						<div class="alert alert-warning">
							Musisz się zalogować, aby dostać się do bazy<br />
						</div>
					</c:if> --%>
				</div>
				<div class="well">
					<form action="<c:url value="/j_spring_security_check"></c:url>"
						method="post">
						<fieldset>
							<legend>Login</legend>
							<input type="text" class="input-block-level"
								placeholder="Nazwa użytkownika" name="j_username"> <input
								type="password" value="" class="input-block-level"
								placeholder="Hasło" name="j_password">
							<div class="pull-right">
								<input type="submit" class="btn btn-primary" value="Zaloguj się">
								<a class="btn btn-info" href="<spring:url value="/register"/>"
									role="button" data-toggle="modal">Zarejestruj się</a>
							</div>
						</fieldset>
					</form>
				</div>

				<!-- <div class="accordion" id="questions">
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle btn" data-toggle="collapse"
								data-parent="#questions" href="#who">Who?</a>
						</div>
						<div id="who" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>bootstrap test</p>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle btn" data-toggle="collapse"
								data-parent="#questions" href="#what">What?</a>
						</div>
						<div id="what" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>bootstrap test2</p>
							</div>
						</div>
					</div>

					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle btn" data-toggle="collapse"
								data-parent="#questions" href="#why">Why?</a>
						</div>
						<div id="why" class="accordion-body collapse">
							<div class="accordion-inner">
								<p>bootstrap test3</p>
							</div>
						</div>
					</div>
				</div> -->

			</div>

			<div class="span8">
				<div id="slider" class="carousel slide">

					<div class="carousel-inner">

						<div class="item active">
							<img src="<c:url value="/resource/images/springmvc.png"></c:url>">
							<div class="carousel-caption">
								<h4>Spring MVC</h4>
								<p>Książka do nauki Springa</p>
							</div>
						</div>

						<div class="item">
							<img src="https://placehold.it/850x500">
							<div class="carousel-caption">
								<h4>Headline for image 2</h4>
								<p>Desc for image 2</p>
							</div>
						</div>

						<div class="item">
							<img src="https://placehold.it/850x500">
							<div class="carousel-caption">
								<h4>Headline for image 3</h4>
								<p>Desc for image 3</p>
							</div>
						</div>

					</div>

					<a class="left carousel-control" href="#slider" data-slide="prev">&lsaquo;</a>
					<a class="right carousel-control" href="#slider" data-slide="next">&rsaquo;</a>

				</div>

			</div>

		</div>

		<footer class="row"></footer>

	</div>
	<!-- END OF CONTAINER-->
	<!--
    <div id="register" class="modal hide fade" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
          <h3 id="modalLabel">Register</h3>
      </div>
      
      <div class="modal-body">
        <form>

          <p class="label label-info">Required</p>

          <div class="controls controls-row">
            <input type="text" class="span2" placeholder="First name">
            <input type="text" class="span2" placeholder="Last name">
            
            <select class="span1">
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
            </select>
          </div>
          
          <div class="controls control-group info">
            <input type="text" class="span5" placeholder="username" id="inputinfo">
            <input type="text" class="span5" placeholder="email" id="inputinfo">
            <input type="text" class="span5" placeholder="confirm email" id="inputinfo">
          </div>

          <p class="help-block">My language is: 
            <a href="#" id="tip" rel="tooltip" data-animation="true" data-original-title="Cool Tip" data-placement="right">Tip</a>
          </p>

          <label class="checkbox inline">
            <input type="checkbox">HTML/CSS
          </label>

          <label class="checkbox inline">
            <input type="checkbox">Javascript
          </label>

          <label class="checkbox inline">
            <input type="checkbox">PHP
          </label>

        </form>
      </div>
      
      <div class="modal-footer">
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button class="btn btn-success">Register</button>
      </div>

    </div> <!--POPUP WINDOW-->
	-->
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<script
		src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
	<!--<script src="js/bootstrap.min.js"></script>-->
	<script type="text/javascript">
		$("#tip").tooltip();
	</script>
</body>
</html>